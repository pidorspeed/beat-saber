﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class delboll : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Boll")
        {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
    }
}
