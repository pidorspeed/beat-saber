﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour
{
    public float rmsValue;
    public float pitchValue;
    private AudioSource sourse;
    private float[] samples;
    private float[] spectrum;
    private float sampleRate;
    

    private void Start()
    {
        sourse = GetComponent<AudioSource>();
        samples = new float[1024];
        spectrum = new float[1024];
        sampleRate = AudioSettings.outputSampleRate;

    }

    private void Update()
    {
        AnalyzeSound();
    }
    private void AnalyzeSound()
    {
        sourse.GetOutputData(samples, 0);
        int i = 0;
        float sum = 0;
        for (; i < 1024; i++)
        {
            sum += samples[i] * samples[i];
        }
        rmsValue = Mathf.Sqrt(sum / 1024);
        sourse.GetSpectrumData(spectrum,0,FFTWindow.BlackmanHarris);

        float maxV = 0;
        var maxN = 0;
        for (i = 0; i < 1024; i++)
        { // find max 
            if (!(spectrum[i] > maxV) || !(spectrum[i] > 0.0f))
                continue;

            maxV = spectrum[i];
            maxN = i; // maxN is the index of max
        }
        float freqN = maxN; // pass the index to a float variable
        if (maxN > 0 && maxN < 1024 - 1)
        { // interpolate index using neighbours
            var dL = spectrum[maxN - 1] / spectrum[maxN];
            var dR = spectrum[maxN + 1] / spectrum[maxN];
            freqN += 0.5f * (dR * dR - dL * dL);
        }
        pitchValue = freqN * (sampleRate / 2) / 1024; // convert index to frequency
    }

}